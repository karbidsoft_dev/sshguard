FROM alpine:3.2
MAINTAINER Sergey Visman <admin@sergeyvisman.ru> (@visman)
ADD whitelist /tmp/whitelist
RUN apk add --update iptables ip6tables sshguard && rm -rf /var/cache/apk/* && \
    cat /tmp/whitelist >> /etc/hosts.allow
ENTRYPOINT ["/usr/sbin/sshguard", "-p 87400 -s 87400 -w /etc/hosts.allow"]
